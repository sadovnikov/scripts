#!/usr/bin/perl -w

# Deleted directory if it contains hidden files/directories only
# See complete description on http://jv-ration.com/2014/01/merge-svn-directories/

my $dir = $ARGV[0];

opendir (DIR ,"$dir") or die "$dir could not be opened: $!";

while (defined ( $file = readdir DIR ) ) {
                next if $file =~ /^\./; # will skip everything that starts with . (DOT)
                $filecount{$dir}++;     # start counting as soon as there is a "real" file or directory
        }

if ( defined $filecount{$dir} )  {
} else {
    print "deleting $dir ...\n";
    system("rm -rf $dir");
}