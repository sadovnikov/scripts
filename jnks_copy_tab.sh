#!/bin/sh -e
# Copies one build job from one Jenkins to another
# see complete description on http://jv-ration.com/2014/01/copying-jenkins-jobs

die () {
    echo >&2 "$@"
    exit 1
}

usage=$"This script copies complete tab of build configurations from one Jenkins server to another.\n\
Four parameters are:\n\
1) name of the tab to copy,\n\
2) Source Jenkins URL\n\
3) Target Jenkins URL\n\
4) (optional) path to jenkins-cli.jar file. If not given, assumed to be found in the current directory\n\
Example: \n\
$0 \"CCD\" http://localhost:8080/  http://dev.jenkins:8080/ /Users/Shared/Jenkins/Home/war/WEB-INF\n\
"

# Reading parameters
[ "$#" -ge 3 ] || die "Wrong parameters. $usage"
tabname=$(echo ${1/ /%20})
serverfrom=$2
serverto=$3
jenkins_cli_path=$4
# If not set, set JENKINS_HOME 
if [ ! $jenkins_cli_path ]
then
  jenkins_cli_path=$(pwd)
fi  

# Finding bash for copying individual jobs
copyjob=$(echo "`dirname $0`/jnks_copy_job.sh")

# finding wget or curl
http_get=$(which wget)
if [ ! $http_get ]
then
  http_get=$(which curl)
  if [ ! $http_get ]
  then
  	die "Neither wget nor curl found on the path"
  else
    http_get=$(echo $http_get "--silent")
  fi
fi 

# Reading jobs of the tab
echo "Retrieving data using $http_get ${serverfrom}view/$tabname/api/json?pretty=true" 
eval "$http_get ${serverfrom}view/$tabname/api/json?pretty=true" | grep "      \"name\" : \"" | awk -v f=" $serverfrom " -v t="$serverto " -v p="$jenkins_cli_path" -v e="$copyjob " 'BEGIN { FS = "[:,]+" }; { system(e $2 f t p) }'
