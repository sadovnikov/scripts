#!/bin/sh -e
# Copies one build job from one Jenkins to another
# see complete description on http://jv-ration.com/2014/01/copying-jenkins-jobs

die () {
    echo >&2 "$@"
    exit 1
}

usage=$"This script copies build configuration from one Jenkins server to another.\n\
Four parameters are:\n\
1) JOBNAME to copy,\n\
2) Source Jenkins URL\n\
3) Target Jenkins URL\n\
4) path to jenkins-cli.jar file\n\
Example: \n\
$0 \"Playground CI\" http://localhost:8080/  http://dev.jenkins:8080/ /Users/Shared/Jenkins/Home/war/WEB-INF\n\
"

[ "$#" -eq 4 ] || die "Wrong parameters. $@ $usage"
jobfrom=$1
serverfrom=$2
serverto=$3
jenkins_cli_path=$4

jobto=$1

cli_cmd_prefix=$( echo "java -jar" $jenkins_cli_path"/jenkins-cli.jar -s")

get_job_cmd=$(echo $cli_cmd_prefix $serverfrom get-job \"$jobfrom\" "2>/dev/null")
create_job_cmd=$(echo $cli_cmd_prefix $serverto create-job \"$jobto\")
copy_job_cmd=$(echo $get_job_cmd "|" $create_job_cmd) 

echo Copying $serverfrom \"$jobfrom\" to $serverto \"$jobto\"
eval "$copy_job_cmd"
echo ..done
