#!/bin/sh -e
# Deletes one build job from one Jenkins
# see complete description on http://jv-ration.com/2014/01/copying-jenkins-jobs

die () {
    echo >&2 "$@"
    exit 1
}

usage=$"This script deletes build configuration from the Jenkins server.\n\
Four parameters are:\n\
1) JOBNAME to delete,\n\
2) Jenkins URL\n\
3) path to jenkins-cli.jar file\n\
Example: \n\
$0 \"Playground CI\" http://localhost:8080/  /Users/Shared/Jenkins/Home/war/WEB-INF\n\
"

[ "$#" -eq 3 ] || die "Wrong parameters. $@ $usage"
jobfrom=$1
serverfrom=$2
jenkins_cli_path=$3

jobto=$1

cli_cmd_prefix=$( echo "java -jar" $jenkins_cli_path"/jenkins-cli.jar -s")

del_job_cmd=$(echo $cli_cmd_prefix $serverfrom delete-job \"$jobfrom\")

echo Deleting $serverfrom \"$jobfrom\"
eval "$del_job_cmd"
echo ..done
