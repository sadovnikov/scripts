#!/bin/sh -e
# remove CR from the given file(s)
# see complete description on http://jv-ration.com/XXXXXXX

die () {
    echo >&2 "$@"
    exit 1
}

if [ -z "$1" ]
then
  die "Usage: `basename $0` file_to_convert \n     You can also xargs multiple file by: ls *.php | xargs `basename $0`"
fi

for toconv in "$@"
do		
	cat $toconv | tr -d '\015' > $toconv.tmp
	rm $toconv
	mv $toconv.tmp $toconv
done