#!/bin/sh -e
# Copies one build job from one Jenkins to another
# see complete description on http://jv-ration.com/2014/01/copying-jenkins-jobs

die () {
    echo >&2 "$@"
    exit 1
}

usage=$"This script deletes all build configurations from the given view (tab).\n\
Four parameters are:\n\
1) name of the tab to empty,\n\
2) Source Jenkins URL\n\
3) (optional) path to jenkins-cli.jar file. If not given, assumed to be found in the current directory\n\
Example: \n\
$0 \"CCD\" http://localhost:8080/  http://dev.jenkins:8080/ /Users/Shared/Jenkins/Home/war/WEB-INF\n\
"

# Reading parameters
[ "$#" -ge 3 ] || die "Wrong parameters. $usage"
tabname=$(echo ${1/ /%20})
serverfrom=$2
jenkins_cli_path=$3
# If not set, set JENKINS_HOME 
if [ ! $jenkins_cli_path ]
then
  jenkins_cli_path=$(pwd)
fi  

# Finding bash for copying individual jobs
deletejob=$(echo "`dirname $0`/jnks_del_job.sh")

# finding wget or curl
http_get=$(which wget)
if [ ! $http_get ]
then
  http_get=$(which curl)
  if [ ! $http_get ]
  then
  	die "Neither wget nor curl found on the path"
  else
    http_get=$(echo $http_get "--silent")
  fi
fi 

# Reading jobs of the tab
echo "Retrieving data using $http_get ${serverfrom}view/$tabname/api/json?pretty=true" 
eval "$http_get ${serverfrom}view/$tabname/api/json?pretty=true" | grep "      \"name\" : \"" | awk -v f=" $serverfrom " -v p="$jenkins_cli_path" -v e="$deletejob " 'BEGIN { FS = "[:,]+" }; { system(e $2 f p) }'
