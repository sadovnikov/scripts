#!/bin/sh -e
# Empties current directory and extracts single file/folder from svn
# see complete description on http://jv-ration.com/2013/11/modify-single-file-in-svn-tree

die () {
    echo >&2 "$@"
    exit 1
}

usage=$"This scripts extract single file/folder from SVN repository into the current directory,\n\
allowing to modify it locally without extracting all other sibling files and directories\n\
\n\
$0 svn_uri\n\
"

[ "$#" -ge 1 ] || die "SVN URI is not provided. $usage"
fullUri=$1

# Testing is svn CLI is present
svnRun=`which svn 2> /dev/null`
if [ ! $svnRun ]
then
  die "svn CLI is not found on PATH"
fi  

# determine parent folder and file from the give URI
svnFile=${fullUri##*/}
svnParent=${fullUri%%/${svnFile}}

# remove .svn and the file if they exist
rm -rf .svn
rm -f $svnFile

# extract the file
svn checkout -q --depth=empty $svnParent .
svn update $svnFile

echo "\n\
After $svnFile is changed locally commit it using\n\
  svn commit -m \"your update description\""
